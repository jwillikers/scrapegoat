package scrapegoat

// Parser parses a given string, extracting objects of interest and returning an error if appropriate
type Parser interface {
	Parse(*string) (interface{},
		error)
}
