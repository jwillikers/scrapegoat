package scrapegoat

import (
	"context"
)

// Scraper scrapes a web page
type Scraper interface {
	Scrape(context.Context, string) (interface{}, error)
}
