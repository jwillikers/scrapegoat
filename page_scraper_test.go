package scrapegoat

import (
	"context"
	"errors"
	"testing"
)

type MockCdpPuller struct {
	html string
	err  error
}

func (p *MockCdpPuller) Pull(ctx context.Context, url string) (string, error) {
	return p.html, p.err
}

func NewMockCdpPuller(html string, err error) *MockCdpPuller {
	p := new(MockCdpPuller)
	p.html = html
	p.err = err
	return p
}

type MockParser struct {
	i   interface{}
	err error
}

func NewMockParser(i interface{}, err error) *MockParser {
	p := new(MockParser)
	p.i = i
	p.err = err
	return p
}

func (p *MockParser) Parse(source *string) (interface{}, error) {
	return p.i, p.err
}

func TestNewPageScraper(t *testing.T) {
	pul := new(MockCdpPuller)
	par := new(MockParser)
	scr := NewPageScraper(pul, par)
	if nil == scr {
		t.Error("the new PageScraper should not be nil")
	}
	if pul != scr.pul {
		t.Errorf("the PageScraper's puller should be '%v' not '%v'", pul, scr.pul)
	}
}

func TestScrape(t *testing.T) {
	t.Run("main", func(t *testing.T) {
		scr := new(PageScraper)
		scr.pul = new(MockCdpPuller)
		scr.par = new(MockParser)
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		_, err := scr.Scrape(ctx, "url")
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
	})
	t.Run("error=puller", func(t *testing.T) {
		scr := new(PageScraper)
		msg := "puller failed to pull web page"
		scr.pul = NewMockCdpPuller("", errors.New(msg))
		scr.par = new(MockParser)
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		_, err := scr.Scrape(ctx, "url")
		if nil == err {
			t.Error("an error should have been returned")
		}
		if msg != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", msg, err.Error())
		}
	})
}
