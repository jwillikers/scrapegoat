package scrapegoat

import (
	"context"

	"bitbucket.org/athrun22/pulpg"
)

// PageScraper scrapes a web page.
type PageScraper struct {
	pul pulpg.Puller
	par Parser
}

// NewPageScraper creates a new PageScraper with the given Puller and Parser.
func NewPageScraper(pul pulpg.Puller, par Parser) *PageScraper {
	s := new(PageScraper)
	s.pul = pul
	s.par = par
	return s
}

// Scrape pulls a web page's html and parses the objects from it.
func (s *PageScraper) Scrape(ctx context.Context, url string) (interface{}, error) {
	html, err := s.pul.Pull(ctx, url)
	if err != nil {
		return nil, err
	}
	return s.par.Parse(&html)
}
