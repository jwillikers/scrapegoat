scrapegoat
===
scrapegoat is a simple web page scraping interface and implementation library. Just want a way to simply scrape a web page? This is it.

Getting Started
---
Using this library is fairly straightforward.

### How It Works
There is a [Scraper](./scraper.go) interface with a single function, Scrape. This function returns a pointer to a string which is the html of the page.

Currently, there is only one implemented scraper, [CdpScraper](./cdp_scraper.go), which lightly wraps chromedp in order to scrape a dynamically populated web page. Controlling Chromedp is primarily left to the user to make this application as lightweight as possible. This means all start up, shutdown, and currently - but subject to change - navigation code must be written in the user's application.

A static implementation using go's net library and more dynamic scrapers based off of selenium, phantom-js, and the like will, with luck, be added to allow for more options.

### In the Wild
-   An example that currently uses scrapegoat's Scraper interface is the [sitechg](https://bitbucket.org/athrun22/sitechg/src/develop/) project.
-   An example that uses the CdpScraper implementation is [sirius](https://bitbucket.org/athrun22/sirius/src/develop/) which scrapes dynamically populated currently playing song names and artists from a SiriusXM channel.

Please Use Responsibly
---
Do *NOT* use this program irresponsibly or nefariously. Using the chromedp package in the CdpScraper with short timeouts can cause detrimental traffic on a website. Use in accordance with the website's rules and regulations.

Built With
---
-   [chromedp](https://github.com/chromedp/chromedp) - a faster, simpler way to drive browsers without external dependencies

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
